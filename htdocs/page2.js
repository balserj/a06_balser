function fibonacci(num) {
        if (num <= 1)
        {
            return num;
        }
        return fibonacci(num-1) + fibonacci(num-2);
    }
    var nums = [];
    for(var q = 0; q < 20; q++) {
        nums.push(fibonacci(q));
    }
    document.write(nums.join());